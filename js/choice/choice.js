class Choice{
	constructor(json){
		
		
	}

	static go(json){
		this.file = json;

		//temporary
		localStorage.clear();

		this.setStyle();
		this.setLayout();
		this.deploySchema(this.getSchema(this.file).current,this.getSchema(this.file).level);
		
	}

	static getSettings(json){
		return json.values;
	}

	static getSchema(json){
		return json.schema;
	}

	static setLayout(){
		var template = this.file.template;
		document.body.innerHTML = template.html;
	}

	static setStyle(){

		var css = this.file.template.css;
		var head = document.head || document.getElementsByTagName('head')[0];
		var style = document.createElement("style");
		style.type='text/css';
		
		if (style.styleSheet){
		  style.styleSheet.cssText = css;
		} else {
		  style.appendChild(document.createTextNode(css));
		}
		
		head.appendChild(style);

		
	}

	static supportPage(){
		
	}

	static lastPage(){
		var schema = this.file;
		
		document.body.style.backgroundColor = schema.last.background_color;

		var quiz = document.getElementById('quiz');
		quiz.innerHTML = '';
		var button = document.getElementById('butt');
		button.innerHTML = 'Comanda';
		button.removeAttribute('onclick');

		var keys=[];

		for(var key in JSON.parse(localStorage.products).products){
		 	keys.push(key);
		 	var newElem = document.createElement('div');
		 	newElem.id = key;
		 	newElem.className = 'col-lg-12 col-md-12 col-sm-12 col-xs-12';
		 	var title = document.createElement('h4');
		 	title.innerHTML = JSON.parse(localStorage.products).products[key];
		 	var sliderDiv = document.createElement('div');
		 	sliderDiv.className = 'col-lg-12 col-md-12 col-sm-12 col-xs-12';
		 	var sliderInput = document.createElement('input');
		 	sliderInput.id = 'sl'+key;
		 	sliderInput.type='text';
		 	sliderInput.setAttribute('data-slider-min','0');
		 	sliderInput.setAttribute('data-slider-max','1000');
		 	sliderInput.setAttribute('data-slider-step','1');
		 	sliderInput.setAttribute('data-slider-value','0');
			var sliderSpan = document.createElement('span');
			sliderSpan.id = 'sl'+key+'CurrentSliderValLabel';
			var secondSliderSpan = document.createElement('span');
			secondSliderSpan.id = 'sl'+key+'SliderVal';

		 	quiz.appendChild(newElem).appendChild(title);
		 	quiz.appendChild(newElem).appendChild(sliderDiv).appendChild(sliderInput)
		 		.appendChild(sliderSpan).appendChild(secondSliderSpan);
	
		}

	 	for(var k in keys){
			var slider = new Slider('#sl'+keys[k]);
			slider.on("slide", function(slideEvt) {
				$("#sl"+keys[k]+"SliderVal").text(slideEvt.value);
			});
	 	}

	}

	static deploySchema(level,schema){
		for(var key in schema){
			this.createSchema(level,key,schema[key].values);
		}
		$('.boxes').fadeIn('slow');
	}

	static createSchema(level,id,values){
		
		
		if(level === "0"){
			document.getElementById('title').innerHTML = 'Alegeti Serviciile Dorite';
		}else{
			document.getElementById('title').innerHTML = values.name;
		}

		var hover = this.file.template.hover_style;

		var answers = document.getElementById('answers');

		var box = document.createElement('div');
		box.className = 'boxes col-lg-2 col-md-3 col-sm-4 col-xs-6';
		

		var a = document.createElement('a');
		
		if(hover === 1){
			a.className = 'cmn-t-shake';
		}else if(hover === 2){
			a.className = 'cmn-t-scale';
		}
		
		a.setAttribute('data-target',id);
		a.setAttribute('data-code',id);
		a.setAttribute('data-checked',false);

		if(values.background_image.length == 0){
		
			var icon = document.createElement('div');
			
			if(hover === 3){
				icon.className = 'answer-icon cmn-t-border-radius';
			}else{
				icon.className = 'answer-icon';
			}	

			icon.style.backgroundColor = values.background_color;

			var icon_parent = document.createElement('div');
			icon_parent.setAttribute('onclick','Choice.select("'+level+'","'+id+'")');
			icon_parent.id = id;

			var i = document.createElement('i');
			i.className = values.default_icon;

			var label = document.createElement('span');
			label.className = 'answer-label';
			label.innerHTML = values.name;

			answers.appendChild(box).appendChild(a)
			.appendChild(icon).appendChild(icon_parent).appendChild(i);
			
			answers.appendChild(box).appendChild(a)
			.appendChild(label);
		
		}else{

			var icon = document.createElement('div');
			
			
			if(hover === 3){
				icon.className = 'answer-icon use-image cmn-t-border-radius';
			}else{
				icon.className = 'answer-icon use-image';	
			}
			
			icon.style.backgroundColor = values.background_color;

			var icon_parent = document.createElement('div');
			icon_parent.style.backgroundImage = values.background_image; 
			icon_parent.setAttribute('onclick','Choice.select("'+level+'","'+id+'")');
			icon_parent.id = id;
			var struct = document.createElement('div');

			var label = document.createElement('span');
			label.className = 'answer-label';
			label.innerHTML = values.name;

			answers.appendChild(box).appendChild(a)
			.appendChild(icon).appendChild(icon_parent).appendChild(struct);
			
			answers.appendChild(box).appendChild(a)
			.appendChild(label);

		}	
		$('.boxes').css('display','none');
		
	}

	static select(level,id){
		var elem = document.getElementById(id);
		
		if(elem.childNodes.length == 2){
			elem.removeChild(elem.childNodes[1]);	
			elem.style.borderStyle = 'none';
			elem.parentElement.style.opacity = "0.6";
		}else{
			var check = document.createElement('div');
			check.id = level;
			elem.appendChild(check);
			elem.style.border = '4px solid #ff7f00';	
			elem.parentElement.style.opacity = "1";
		}
		
	}

	static next(){

		var code=[];
		var level;
		var boxes = document.getElementsByClassName('boxes');
		for(var x=0;x<boxes.length;x++){
			if(boxes[x].firstElementChild.firstElementChild.firstElementChild.childNodes.length >= 2){
				code.push(boxes[x].firstElementChild.firstElementChild.firstElementChild.id);
				level = boxes[x].firstElementChild.firstElementChild.firstElementChild.childNodes[1].id;
			}	
		}
		
		if(code != ''){
			this.setCode(level,code);
		}else{
			return false;
		}
	}

	static setCode(level,value){

		if(localStorage.getItem("code") === null){
			var code = {
				'code':[value]
			};

			localStorage.setItem('code',JSON.stringify(code));

			this.nextLevel(level,value);
			
		}else{
			
			var newcode=[];
			for(var key in JSON.parse(localStorage.code).code){
				newcode[key] = JSON.parse(localStorage.code).code[key];		
			}
			
			newcode.push(value);
			var code = {
				'code':newcode
			};		
			
			localStorage.setItem('code',JSON.stringify(code));
			
			this.nextLevel(level,value);
			
		}
	}

	static eraseBoxes(){
		var container = document.getElementById('answers');
		container.innerHTML = '';
		
	}

	static checkLast(object){

		if(object.constructor === Array){
			
			
			var last=[];
			var noLast=[];

			for(var key in object){
				if(object[key].last == true){
					last.push(object[key]);
				}else{
					noLast.push(object[key]);
				}
			}
			//console.log(last,noLast);
			if(last.length > 0){

				
				
				if(localStorage.getItem("products") == null){
					var products=[];

					for(var key in last){
						products.push(last[key].product);
					}

					var code = {
						'products':products
					};

					localStorage.setItem('products',JSON.stringify(code));
				}else{
					var products=[];

					for(var key in JSON.parse(localStorage.products).products){
						if(JSON.parse(localStorage.products).products[key] === null){continue;}
						products[key] = JSON.parse(localStorage.products).products[key];
					}

					for(var key in last){
						products.push(last[key].product);
					}

					var code = {
						'products':products
					};

					localStorage.setItem('products',JSON.stringify(code));
				}	

				return [true,last];
			}else{
				return [false,noLast];
			}
		}else{
			if(object.last == true){
				
				if(localStorage.getItem("products") == null){
					var code = {
						'products':[object.product]
					};


					localStorage.setItem('products',JSON.stringify(code));

				}else{

					var newProducts=[];
					
					for(var key in JSON.parse(localStorage.products).products){
						if(JSON.parse(localStorage.products).products[key] === null){continue;}
						newProducts[key] = JSON.parse(localStorage.products).products[key];
					}

					newProducts.push(object.product);

					var code = {
						'products':newProducts
					};		

					localStorage.setItem('products',JSON.stringify(code));

				}

				return true;
			}else{
				return false;
			}
		}
			
	}

	static findObjects(values,schema){
		var objects=[];
		var current;
		
		var split = values[0].split('');
		var temp='';
		//console.log(split);
		for(var i=0;i<split.length-1;i++){
			temp += split[i];
			
			if(i==0){
				current = schema[temp];
			}else if(i == split.length - 1){
				current = current.level;
			}else{
				current = current.level[temp];
			}	
		}
		
		
		var removers=this.arr_obj_diff(current.level,values);
		
		for(var i in removers){
			delete current.level[removers[i]];
		}

		//console.log(current);

		for(var key in current.level){
			objects.push(current.level[key]);
		}
		
		return [current,objects];

	}

	static checkLastFirst(obj){
		var last=[];
		var noLast=[];
		var products=[];

		for(var k in obj){
			if(obj[k].last == true){
				last.push(obj[k]);
				products.push(obj[k].product);
			}else{
				noLast.push(obj[k]);
			}
		}	

		var code = {
			'products':products
		};

		localStorage.setItem('products',JSON.stringify(code));

		return noLast;
		
	}

	static nextLevel(currentLevel,value){
		console.log(value);
		var schema = this.getSchema(this.file).level;
		value = this.cleanArray(value);
		if(value.length > 1){
			
			if(currentLevel == 0){
				var objects=[];

				for(var key in value){
					objects.push(schema[value[key]]);
				}
				
				//var nolast = this.checkLastFirst(objects);
				
				/*if(nolast.length == 0){
					//this.eraseBoxes();
					//this.deploySchema(objects[0].current,objects[0].level);
					localStorage.removeItem("code");
					localStorage.setItem('location',JSON.stringify({'location':'last_page'}));
					//assistance
					this.lastPage();
				}else{
					for(var x=0;x < nolast.length;x++){
						if(x == nolast.length - 1){
							this.findNode(true,nolast[x],1);
						}else{
							this.findNode(true,nolast[x]);	
						}
					}	
				}*/

				this.checkLast(objects);
				this.eraseBoxes();
				this.deploySchema(objects[0].current,objects[0].level);
			}else{
				//console.log('level +1');
				var values=[];

				for(var key in value){
					values.push(value[key]);
				}

				var results = this.findObjects(values,schema);
				var check = this.checkLast(results[1]);
				
					
				if(check[0] == true){
					for(var x=0;x < check[1].length;x++){
						if(x == check[1].length - 1){
							this.findNode(true,check[1][x],1);
						}else{
							this.findNode(true,check[1][x]);	
						}
					}
					
				}else{
					this.eraseBoxes();
					this.deploySchema(check[1][0].current,check[1][0].level);
				}
					
			}
		}else{
			if(currentLevel == 0){
				
				if(this.checkLast(schema[value[0]])){
					this.findNode(false,schema[value[0]]);
				}
				this.eraseBoxes();
				this.deploySchema(schema[value[0]].current,schema[value[0]].level);
			}else{

				var current = this.findObject(value);
						
				if(this.checkLast(current)){
					this.findNode(false,current);
				}else{
					this.eraseBoxes();
					this.deploySchema(current.current,current.level);
				}
			}
			
		}
	}

	static findNode(loop,object,lastEl=null){
		
		if(loop === true){
			var code = JSON.parse(localStorage.code).code;
			var key = code[object.current-1].indexOf(object.key);
			
			//console.log(code[object.current-1],code);
			code[object.current-1].splice(key,1);

			var remain=[];

			for(var i in code){
				remain.push(code[i]);
			}

			code={
				'code':remain
			};

			localStorage.setItem('code',JSON.stringify(code));
			//console.log(localStorage);
			if(lastEl != null){
				var current = this.findLevel();
				//console.log(remain[object.current-1]);
				this.nextLevel(current,remain[object.current-1]);
			}
		}else{
			var code = JSON.parse(localStorage.code).code;
			//console.log(code[0],code[1],code[2],code[3],code[4]);
			code[object.current-1].shift();
			//console.log(code[0],code[1],code[2],code[3],code[4]);
			
			for(var i=code.length-1;i>=0;i--){
				if(code[i].length == 0){
					code.splice(i,1);
					
				}else{
					code[i].shift();
					if(code[i].length == 0){
						code.splice(i,1);
					}else{
						break;
					}
				}
			}

			var next = code[code.length-1];

			if(code.length == 0){
				localStorage.removeItem("code");
				localStorage.setItem('location',JSON.stringify({'location':'last_page'}));
				this.lastPage();
			}else{
				
				code={
					'code':code
				};
				localStorage.setItem('code',JSON.stringify(code));
				var current = this.findLevel();
				this.nextLevel(current,next);
			}
		}
		

	}
	

	static findLevel(){	
		return (JSON.parse(localStorage.code).code.length - 1);
	}

	static findObject(value){
		var schema = this.getSchema(this.file).level;
		var split = value[0].split('');
		var current;
		var temp='';
		
		for(var i=0;i<split.length;i++){
			temp += split[i];
			if(i==0){
				current = schema[temp];
			}else{
				current = current.level[temp];
			}	
		}
		
		return current;
	}



	static arr_obj_diff(obj, a2) {

	    var a = [], diff = [];


	    for(var key in obj){
	    	a[key] = true;
	    }

	    for (var i = 0; i < a2.length; i++) {
	        if (a[a2[i]]) {
	            delete a[a2[i]];
	        } else {
	            a[a2[i]] = true;
	        }
	    }

	    for (var k in a) {
	        diff.push(k);
	    }

	    return diff;
	}
	
	static cleanArray(actual) {
		var newArray = new Array();
		  	for (var i = 0; i < actual.length; i++) {
		    	if (actual[i]) {
		    		newArray.push(actual[i]);
		    	}
		  	}
		return newArray;
	}
	
}

